An example of a vertical scrolling map with pygame

There are some assumptions on the scrolling image for the seamless looping
effect to work:
  - The image height must be at least twice the height of the
    screen/viewport.
  - The image must have two identical regions, one at the top and one at
    the bottom, of the same height as the screen/viewport.

See the "Viewport" layer in the "road.svg" image.

A png image ready to use can be generated with this command line:

  inkscape --file=road.svg --export-area-page --export-width=800 --export-png=road.png 
